#pragma once
#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <math.h>

#include <iostream>

int SolveLinearEquation(double a, double b, double* root);
void checkEquationPointers(double* x1, double* x2);
void checkEquationCoefficients(double a, double b, double c);
int SolveQuadraticEquation(double a, double b, double c, double* x1, double* x2);

#define SomethingWentWrong 404
#define QuadraticEquationHasInfintRoots -1
#define LinearEquationHasInfintRoots -1
#define LessEnoughToBeZero 1e-8
