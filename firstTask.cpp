#include "firstTask.h"

// a * X + b = 0
inline int SolveLinearEquation(double a, double b, double* root){
  assert(root != NULL);
  if( abs(a) < LessEnoughToBeZero ){
    if( abs(b) < LessEnoughToBeZero ){
      return LinearEquationHasInfintRoots;
    }
    else{
      return 0;
    }
  }
  else{
    *root = -b/a;
    return 1;
  }
  return SomethingWentWrong;
}

inline void checkEquationPointers(double* x1, double* x2){
  assert(x1 != NULL);
  assert(x2 != NULL);
  assert(x1 != x2);
}

inline void checkEquationCoefficients(double a, double b, double c){
  assert(std::isfinite(a));
  assert(std::isfinite(b));
  assert(std::isfinite(c));
}

// a * X^2 + b * X + c = 0
int SolveQuadraticEquation(double a, double b, double c, double* x1, double* x2){
  checkEquationPointers(x1, x2);
  checkEquationCoefficients(a, b, c);

  if( abs(a) < LessEnoughToBeZero ){
    int anountOfRoots = SolveLinearEquation(a, b, x1);
    if( anountOfRoots == LinearEquationHasInfintRoots ){
      return QuadraticEquationHasInfintRoots;
    }
    *x2 = *x1;
    return anountOfRoots;
  }
  else{
    double discr = b * b - 4.0f * a * c;
    if( discr < 0.0 ){
      return 0;
    }
    if( abs(discr) < LessEnoughToBeZero ){
      *x1 = -b/(2.0f * a);
      *x2 = *x1;
      return 1;
    }
    double discrRoot = sqrt(discr);
    *x1 = (-b + discrRoot) / (2.0f * a);
    *x2 = (-b - discrRoot) / (2.0f * a);
    return 2;
  }
  return SomethingWentWrong;
}


int main(){

  printf("# This program solves quadratic equations\n");
  printf("# Enter parametrs a, b, c of quadratic equations a * X^2 + b * X + c = 0\n");
  double a = 0, b = 0, c = 0;
  printf("# a = ");
  scanf("%lf", &a);
  printf("# b = ");
  scanf("%lf", &b);
  printf("# c = ");
  scanf("%lf", &c);

  double x1 = 0, x2 = 0;
  int amountOfRoots = SolveQuadraticEquation(a, b, c, &x1, &x2);

  switch (amountOfRoots){
    case 0:
      printf("No roots\n");
      break;
    case 1:
      printf("x1 = %f\n", x1);
      break;
    case 2:
      printf("x1 = %f, x2 = %f\n", x1, x2);
      break;
    case QuadraticEquationHasInfintRoots:
      printf("Infinite number of solutions. Root is any number\n");
      break;
    default:
      printf("Unexpected number of roots = %d \n", amountOfRoots);
      printf("from a function: \"SolveQuadraticEquation\" \n");
      exit(1);
      break;
  }

  return 0;
}